from django.apps import AppConfig


class ClientCatalogConfig(AppConfig):
    name = 'client_catalog'
