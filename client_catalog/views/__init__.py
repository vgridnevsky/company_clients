"""
Views for the client catalog: list, creation and deletion, etc.
"""


from django.http import JsonResponse
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.views.generic.detail import DetailView
from django_filters.views import FilterView
from django_tables2.views import SingleTableView
from client_catalog.models import Client
from client_catalog.tables import ClientTable
from client_catalog.filters import ClientFilter
from django.views.generic import TemplateView


class IndexView(TemplateView):
    """
    This view simply returns index page.
    """
    template_name = 'index.html'


class ClientList(FilterView, SingleTableView):
    """Returns "List of clients\" page."""
    table_class = ClientTable
    model = Client
    template_name = 'client_list.html'
    filterset_class = ClientFilter


class ClientView(DetailView):
    """Returns data for the individual client."""
    model = Client
    template_name = 'client_view.html'
    context_object_name = 'full_client_data'
    pk_url_kwarg = 'client_id'

    def get_context_data(self, **kwargs):
        """Adds age field to the client data."""
        context = super(ClientView, self).get_context_data(**kwargs)
        context['age'] = self.object.calculate_age()
        return context


class ClientCreateView(CreateView):
    """
    Allows to create a client record.
    """
    model = Client
    template_name = 'client_form.html'
    success_url = '/client/list'
    fields = ["first_name", "last_name", "birth_date", "photo"]


class ClientVoteView(ListView):
    """
    View for client votes.
    Returns voting page on GET request.
    """
    model = Client
    template_name = 'client_vote.html'
    ordering = ['pk']


def del_clients_view(request):
    """
    Removes one or more client records,
    selecting these by list of primary keys.
    """
    deleted_client_pk_list = request.POST.getlist('deleted_pk_list[]')
    try:
        deleted_client_pk_list = [int(pk) for pk in deleted_client_pk_list
                                  if pk.isdigit()]
    except ValueError:
        deleted_client_pk_list = []
    deleted_client_list = Client.objects.filter(
        id__in=deleted_client_pk_list
    )
    deleted_client_list.delete()
    return JsonResponse({'status': 'ok'})
