"""
Contains site API.
ClientList and ClientDetail classes allow to
access client info.
ClientVote allows to vote for a client.
"""

from django.db import transaction
from django.db.models import F
from django.utils.decorators import method_decorator
from rest_framework.views import APIView
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework import generics
from rest_framework import status

from client_catalog.models import Client
from client_catalog.serializers import ClientSerializer


MAX_VOTE_POINTS = 10


class ApiRoot(APIView):
    """
    The entry endpoint of our API.
    """

    def get(self, request):
        """
        Returns index page
        """
        return Response({
            'clients': reverse('client-list', request=request),
        })


class ClientDetail(generics.RetrieveAPIView):
    """
    API endpoint that represents a single client.
    """

    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientList(generics.ListCreateAPIView):
    """
    API endpoint that represents a list of clients.
    """

    serializer_class = ClientSerializer
    queryset = Client.objects.all()

# TODO leave as-is
class ClientVote(APIView):
    """
    Accepts a vote for a client.
    """

    def put(self, request, pk):
        """
        Receives client ID, increments client votes.
        """

        # Use F-expression for update
        with transaction.atomic():
            Client.objects.select_for_update().filter(
                pk=pk,
                points__lt=MAX_VOTE_POINTS
            ).update(points=F('points') + 1)
        # Send updated points in user request
        client = Client.objects.get(pk=pk)
        points = client.points

        return Response(
            {'points': points},
            status=status.HTTP_200_OK
        )


@method_decorator(transaction.atomic, 'put')
class ClientVoteRU(generics.RetrieveUpdateAPIView):
    serializer_class = ClientSerializer

    def get_query_set(self):
        return Client.objects.select_for_update().filter(votes__lt=10)

    def get_object(self):
        return self.get_query_set.get(pk=self.kwargs('pk'))

    def put(self, *args, **kwargs):
        super(ClientVoteRU, self).get_context_data(*args, **kwargs)

        self.object.votes += 1
        self.object.save()