"""
Views for the client catalog XLSX export.
"""
from os import path, pardir

from django.conf import settings
from django.http import HttpResponse, JsonResponse, Http404
from django.shortcuts import render

from client_catalog.tasks import generate_clients_xlsx
from company_clients.celery import celery_app


def clients_export_view(request):
    """
    Shows a template-based page with export-requesting script.
    This script adds a button when XLSX file becomes available.
    """
    return render(request, "client_list_xlsx_download.html", {})


def clients_export_request_view(request):
    """
    Returns Celery task id for a new table.
    """
    celery_request = generate_clients_xlsx.delay()
    task_id = celery_request.task_id
    return JsonResponse({"task-id": task_id})


def clients_export_check_view(request, task_id):
    """
    Return XLSX output state.
    """
    res = celery_app.AsyncResult(task_id)
    return JsonResponse({
        "state": res.state
    })


def clients_export_download_view(request, task_id):
    """
    Returns XLSX file by task_id.
    """
    workbook_folder = path.join(
        path.join(settings.BASE_DIR, pardir),
        'exported_xlsx'
    )
    workbook_filename = '{}.xlsx'.format(task_id)
    try:
        result_file = open(path.join(workbook_folder, workbook_filename), 'rb')
    except IOError:
        raise Http404('Document does not exist')
    response = HttpResponse(
        result_file, content_type='application/force-download'
    )
    response['Content-Disposition'] = 'attachment; filename=export.xlsx'
    return response
