"""
Urls for XLSX-export views.
"""

from django.conf.urls import url
from client_catalog.views.xlsx_export import clients_export_view, \
                                       clients_export_request_view, \
                                       clients_export_check_view, \
                                       clients_export_download_view

urlpatterns = [
    url(r'^$', clients_export_view),
    url(r'^request/$', clients_export_request_view),
    url(r'^check/(?P<task_id>[0-9a-z\-]+)/$', clients_export_check_view),
    url(r'^download/(?P<task_id>[0-9a-z\-]+)/$', clients_export_download_view),
]