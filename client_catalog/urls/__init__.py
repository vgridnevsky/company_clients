"""
Contains url routing for client catalog app.
"""
from django.conf.urls import include, url

from client_catalog.views import ClientView, ClientVoteView, \
                                 ClientCreateView, ClientList, \
                                 del_clients_view, \
                                 IndexView


urlpatterns = [
    # Index
    url(r'^$', IndexView.as_view()),
    # API
    url(r'^api/', include('client_catalog.urls.api')),
    # Client operations
    url(r'^client/(?P<client_id>[0-9]+)/$', ClientView.as_view()),
    url(r'^client/list/$', ClientList.as_view()),
    url(r'^client/new/$', ClientCreateView.as_view()),
    url(r'^client/delete/$', del_clients_view),
    url(r'^client/vote/$', ClientVoteView.as_view()),
    url(r'^client/xlsx_export/', include('client_catalog.urls.xlsx_export')),
]