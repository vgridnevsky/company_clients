"""
Contains classes and functions needed to represent client table with
Django-tables 2.
"""


from django.utils.html import format_html
import django_tables2 as tables
from client_catalog.models import Client


class URLColumn(tables.Column):
    """
    This column contains url to the record.
    """
    def render(self, value):
        return format_html('<a href="{}">#{}</a>', value, value)


class SelectColumn(tables.Column):
    """
    This column contains checkbox to select the record.
    """
    def render(self, value):
        return format_html(
            '<input type="checkbox" data-pk="{}"></input>',
            value
        )


class ImageColumn(tables.Column):
    """
    This column contains a photo to show in the record.
    """
    def render(self, value):
        return format_html(
            '<img src="/static/media/{}" width="200px" />',
            value.url
        )


class ClientTable(tables.Table):
    """
    Table definition for clients catalog.
    """
    select = SelectColumn('Select', accessor='id', orderable=False)
    index = URLColumn('Id', accessor='id')
    photo = ImageColumn()

    # pylint: disable=too-few-public-methods
    class Meta:
        """
        Columns and style for the client list table.
        """
        model = Client
        attrs = {'class': 'paleblue'}
        sequence = (
            'select', 'index', 'first_name', 'last_name', 'birth_date',
            'photo'
        )
