"""
Contains filters for \"Client catalog\" app.
"""
from django_filters import FilterSet
from client_catalog.models import Client


class ClientFilter(FilterSet):
    """
    Filter used in client list.
    """
    class Meta:
        model = Client
        fields = ['first_name', 'last_name']
