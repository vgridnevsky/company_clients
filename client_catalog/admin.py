"""
Admin panel settings for client catalog app.
"""

from django.contrib import admin
from client_catalog.models import Client


class ClientAdmin(admin.ModelAdmin):
    """
    Client list display settings
    """
    list_display = ('id', '_client_name', 'birth_date', '_photo')

    @classmethod
    def _client_name(cls, obj):
        """
        Custom field, returning sum of first and last names.
        """
        return '{} {}'.format(obj.first_name, obj.last_name)

    @classmethod
    def _photo(cls, obj):
        """
        Custom field, returning a photo as img tag.
        """
        return '<a href="/static/media/{}">Open</a>'.format(obj.photo)


admin.site.register(Client, ClientAdmin)
