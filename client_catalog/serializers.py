"""Django rest framework serializers to use with client catalog models."""


from client_catalog.models import Client
from rest_framework import serializers


class ClientSerializer(serializers.HyperlinkedModelSerializer):

    """
    Serializer for a client.
    """

    class Meta:
        model = Client
        fields = ('pk', 'first_name', 'last_name', 'birth_date')


class ClientVoteSerializer(serializers.HyperlinkedModelSerializer):

    """
    Serializer for a client vote.
    """

    class Meta:
        model = Client
        fields = ('pk', 'points')