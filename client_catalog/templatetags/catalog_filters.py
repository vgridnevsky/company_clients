"""
This module contains Django template filter,
used for setting widget classes.
"""


from django import template


register = template.Library()


@register.filter()
def set_field_class(form_field, css_class):
    """Sets a field widget class in template."""
    form_field.field.widget.attrs.update({'class': css_class})
    return form_field


@register.filter()
def set_field_placeholder(form_field, input_placeholder):
    """Sets a placeholder for an input tag."""
    form_field.field.widget.attrs.update({'placeholder': input_placeholder})
    return form_field
