"""
Celery tasks for \"Client catalog\" app.
"""
from __future__ import absolute_import, unicode_literals
from os import path, pardir, makedirs

from django.conf import settings

from celery import shared_task

# Excel export
import openpyxl
from openpyxl.utils import get_column_letter
from openpyxl.styles import Alignment, Font

# DB models
from client_catalog.models import Client


@shared_task
def generate_clients_xlsx():
    """
    Dumps all client records to xlsx file.
    """
    openpyxl_workbook = openpyxl.Workbook()
    current_worksheet = openpyxl_workbook.get_active_sheet()
    current_worksheet.title = 'Client catalog'

    left_alignment = Alignment(
        horizontal='left',
        vertical='bottom',
        text_rotation=0,
        wrap_text=False,
        shrink_to_fit=False,
        indent=0)

    def set_columns(start_row_number):
        """
        Writes column names to the sheet
        """
        row_num = start_row_number
        columns = [
            ('#', 15), ('First name', 20), ('Last name', 20),
            ('Birth date', 25),
        ]

        bold_font = Font(bold=True)

        # pylint: disable=W0612
        for column_number, column in enumerate(columns):
            cell = current_worksheet.cell(
                row=row_num + 1,
                column=column_number + 1
            )
            cell.value = columns[column_number][0]
            cell.alignment = left_alignment
            cell.font = bold_font
            # set column width
            column_letter = get_column_letter(column_number + 1)
            current_worksheet.column_dimensions[column_letter].width = \
                columns[column_number][1]

    def write_clients(start_row_number):
        """
        Writes values from each model to the sheets
        """
        row_num = start_row_number
        for obj in Client.objects.all():
            row_num += 1
            row = [
                obj.pk,
                obj.first_name,
                obj.last_name,
                obj.birth_date
            ]
            for col_num, column in enumerate(row):
                cell = current_worksheet.cell(
                    row=row_num + 1,
                    column=col_num + 1)
                cell.value = column
                cell.alignment = left_alignment

    def save_workbook():
        """Saves workbook to file."""
        workbook_folder = path.join(
            path.join(settings.BASE_DIR, pardir),
            'exported_xlsx'
        )
        workbook_filename = '{}.xlsx'.format(generate_clients_xlsx.request.id)
        workbook_path = path.join(workbook_folder, workbook_filename)
        if not path.exists(workbook_folder):
            makedirs(workbook_folder)
        openpyxl_workbook.save(workbook_path)
        return workbook_filename

    set_columns(0)
    write_clients(1)
    spreadsheet_filename = save_workbook()

    return spreadsheet_filename
