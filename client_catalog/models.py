"""
Models for Client catalog app.
"""


from datetime import date
from django.db import models


class Client(models.Model):
    """
    Contains all required client data.
    """
    first_name = models.CharField('First name', max_length=50)
    last_name = models.CharField('Last name', max_length=50)
    birth_date = models.DateField('Birth date')
    photo = models.ImageField('Photo', upload_to="uploads/%Y/%m/%d/")
    points = models.IntegerField('Points', default=0)

    def calculate_age(self):
        """
        Convert age to birth date.
        """
        today = date.today()
        return today.year - self.birth_date.year - \
               ((today.month, today.day) < \
               (self.birth_date.month, self.birth_date.day))

    def __str__(self):
        return(
            "{} {}. {}\n{}".format(
                self.first_name,
                self.last_name,
                self.birth_date.strftime('%b %d %Y'),
                self.photo)
        )
