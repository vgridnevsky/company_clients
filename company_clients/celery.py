"""
Provides a Celery app for the project.
It simply loads tasks from other apps.
"""
from __future__ import absolute_import, unicode_literals
import os
from celery import Celery


# set the default Django settings module for the 'celery' program.
os.environ.setdefault(
    'DJANGO_SETTINGS_MODULE',
    'company_clients.settings'
)

celery_app = Celery(
    'company_clients',
    broker='amqp://',
    backend='amqp://',
)
celery_app.config_from_object('django.conf:settings', namespace='CELERY')
celery_app.autodiscover_tasks()
